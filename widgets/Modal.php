<?php

namespace app\widgets;

use yii\helpers\Html;
use Yii;

class Modal extends \yii\base\Widget {

    public $opciones = [];
    public $boton = "botonModal";
    public $contenido = "modalContent";

    public function init() {
        $this->opciones = array_merge([
            "title" => "Ejemplo",
            "id" => "modal",
            "size" => 'modal-xl',
            'closeButton' => [
                'label' => 'Cerrar',
                'class' => 'btn btn-danger btn-sm pull-right',
            ],
            'footer' => Html::a('Cerrar', ['#'], ['class' => 'btn btn-danger btn-sm pull-right', 'data-dismiss' => 'modal']),
                ], $this->opciones);
        parent::init();
    }

    public function run() {
        \yii\bootstrap4\Modal::begin($this->opciones);
        echo "<div id='$this->contenido'></div>";
        \yii\bootstrap4\Modal::end();

        // realizado con JQUERY solamente
        /* $this->getView()->registerJs(
          "$(function(){
          $('.$this->boton').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#" . $this->opciones['id'] . "').modal('show').find('#$this->contenido').html(data)
          });
          return false;
          });
          });"); */


        // realizado con JS y utilizar JQUERY solo para el modal de bootstrap4
        $this->getView()->registerJs("
        let a = document.querySelectorAll(\".modalButton\");
        a.forEach(function (boton) {
        boton.addEventListener(\"click\", function (e) {
            e.preventDefault();
            fetch(
                    e.currentTarget.href
                    )
                    .then(response => {
                        if (response.ok) {
                            return response.text();
                        } else {
                            console.log('error');
                        }

                    })
                    .then(valores => {
                        // recupero los datos del servidor
                        datos = valores;
                        // imprimo los datos
                        document.querySelector('#$this->contenido').innerHTML = datos;
                        $('#" . $this->opciones['id'] . "').modal('show');
                    });
        });
    });");
    }

}
