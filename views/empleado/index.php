<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Empleado;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Empleado', ['createp'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'codigo',
            'nif',
            'nombre',
            'apellido1',
            'apellido2',
            //'codigo_departamento',
            'codigoDepartamento.nombre',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Empleado $model, $key, $index, $column) {
                    return Url::toRoute([$action . 'p', 'codigo' => $model->codigo]);
                },
            ],
        ],
    ]);
    ?>


</div>

<div id="salida">

</div>

<script>

    botones = document.querySelectorAll("a");
    botones.forEach(function (b) {
        b.addEventListener("click",
                function (e) {
                    e.preventDefault();
                    fetch(
                            e.currentTarget.href
                            )
                            .then(response => {
                                if (response.ok) {
                                    return response.text();
                                } else {
                                    console.log('error');
                                }

                            })
                            .then(valores => {

                                // recupero los datos del servidor

                                datos = valores;
                                // imprimo los datos
                                document.querySelector('#salida').innerHTML = datos;
                            });
                });
    });
</script>
