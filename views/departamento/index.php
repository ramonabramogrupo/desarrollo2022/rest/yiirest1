<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use app\models\Departamento;
use app\widgets\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departamentos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departamento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Departamento', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'codigo',
            'nombre',
            'presupuesto',
            'gastos',
            [
                'label' => 'REST',
                'format' => 'raw',
                'content' => function ($modelo) {
                    return Html::a($modelo->codigo, ['depart/'], ['class' => 'modalButton']);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Departamento $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model->codigo]);
                },
                'buttonOptions' => ['class' => 'modalButton'],
            ],
        ],
    ]);
    ?>
</div>

<?php
echo app\widgets\Modal::widget([
    "boton" => "modalButton"
]);
?>





